import { createAction, props } from "@ngrx/store";
import { Member } from "src/app/common/models/member";

export const FetchHierarchyForId = createAction(
    '[Hierarchy] FETCH_HIERARCHY_FOR_ID',
    props<{ id: string }>()
  );
  export const FetchHierarchyForIdSuccess = createAction(
    '[Hierarchy] FETCH_HIERARCHY_FOR_ID_SUCCESS',
    props<{ total: number, hierarchy: Array<Member> }>()
  );
  export const FetchHierarchyForIdFailure = createAction('[Hierarchy] FETCH_HIERARCHY_FOR_ID_FAILURE');