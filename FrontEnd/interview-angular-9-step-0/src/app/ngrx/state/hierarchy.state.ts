import { Member } from './../../common/models/member';
import { createFeatureSelector, createSelector } from "@ngrx/store";

export interface HierarchyState {
    loading: boolean;
    loaded: boolean;
    total: number;
    hierarchy: Member[];
    selectedMember:Member;
  }
  
  export const initialHierarchyState: HierarchyState = {
    loading: false,
    loaded: false,
    total: 0,
    hierarchy:[],
    selectedMember : null
  };
  
  export const membersFeatureSelector = createFeatureSelector<HierarchyState>('hierarchy');
  
  export const getHierarchySelector = createSelector(membersFeatureSelector, state => state.hierarchy);
