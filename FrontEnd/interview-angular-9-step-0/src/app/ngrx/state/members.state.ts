import {Member} from '../../common/models/member';
import {createFeatureSelector, createSelector} from '@ngrx/store';
import {Filters} from '../../common/models/filters';
import {AppConfig} from '../../app.config';
import { ListItem } from 'src/app/common/models/list-item';


export interface MembersState {
  loading: boolean;
  loaded: boolean;
  total: number;
  members: Member[];
  selectedMember:ListItem;
}

export const initialMembersState: MembersState = {
  loading: false,
  loaded: false,
  total: 0,
  members: [],
  selectedMember:null
};

export const membersFeatureSelector = createFeatureSelector<MembersState>('members');

export const getMembersSelector = createSelector(membersFeatureSelector, state => state.members);

export const getSelectedMemberSelector = createSelector(membersFeatureSelector, state => state.selectedMember);

