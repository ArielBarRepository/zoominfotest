import { Member } from './../../../common/models/member';
import { Component, Input, OnInit, OnChanges, SimpleChanges } from '@angular/core';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import { ListItem } from 'src/app/common/models/list-item';
import { getHierarchySelector, HierarchyState } from 'src/app/ngrx/state/hierarchy.state';
import { FetchHierarchyForId } from 'src/app/ngrx/action/hierarchy.actions';
import { Router } from '@angular/router';

@Component({
  selector: 'zi-hierarchy',
  templateUrl: './hierarchy.component.html',
  styleUrls: ['./hierarchy.component.scss']
})
export class HierarchyComponent implements OnInit,OnChanges {

  @Input() selectedMember: ListItem;
  hierarchy$: Observable<Member[]>;

  constructor(private router: Router,private hierarchyStore: Store<HierarchyState>) { }
  ngOnChanges(changes: SimpleChanges): void {
    if(this.selectedMember){
      this.hierarchyStore.dispatch(FetchHierarchyForId({id: this.selectedMember.id}));
    }
  }

  ngOnInit(): void {
    this.hierarchy$ = this.hierarchyStore.select(getHierarchySelector);
  }

  goToMember(item){
    this.router.navigate( ['/app/'+item.id]);
  }
}
