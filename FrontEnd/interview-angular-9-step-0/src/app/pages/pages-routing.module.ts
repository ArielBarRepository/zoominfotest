import { MembersComponent } from './members/members.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule, PreloadAllModules } from '@angular/router';
import { PagesComponent } from './pages.component';
import { MemberInformationComponent } from './member-information/member-information.component';


const routes: Routes = [
  {
    path: '',
    component: PagesComponent,
    children: [
      {
        path: '',
        component: MembersComponent,
        outlet: 'sub'
      }
    ]
  },
  {
    path: ':id',
    component: PagesComponent,
    children: [
      {
        path: '',
        component: MemberInformationComponent,
        outlet: 'sub'
      }
    ]
  }
];


@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PagesRoutingModule { }
