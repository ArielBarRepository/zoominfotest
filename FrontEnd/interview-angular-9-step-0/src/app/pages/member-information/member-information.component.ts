import { Member } from './../../common/models/member';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { MembersService } from '../services/members.service';

@Component({
  selector: 'app-member-information',
  templateUrl: './member-information.component.html',
  styleUrls: ['./member-information.component.scss']
})
export class MemberInformationComponent implements OnInit {
  selectedMember: Member;
  error: boolean = false;
  constructor(private route: ActivatedRoute, private router: Router, private membersService: MembersService) { }

  ngOnInit(): void {
    this.route.paramMap.subscribe(paramMap => {
      const id = paramMap.get('id');
      this.membersService.getMemberById(id).subscribe((member) => {
        this.selectedMember = member;
      },
        err => this.error = true)
    })
  }

  back() {
    this.router.navigate(['/app']);
  }

}
