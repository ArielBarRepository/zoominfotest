import { createFeatureSelector, createSelector } from "@ngrx/store";
import { Member } from "src/app/common/models/member";

export interface HierarchyState {
    loading: boolean;
    loaded: boolean;
    total: number;
    hierarchy: Member[];
  }
  
  export const initialHierarchyState: HierarchyState = {
    loading: false,
    loaded: false,
    total: 0,
    hierarchy:[]
  };
  
  export const membersFeatureSelector = createFeatureSelector<HierarchyState>('hierarchy');
  
  export const getHierarchySelector = createSelector(membersFeatureSelector, state => state.hierarchy);