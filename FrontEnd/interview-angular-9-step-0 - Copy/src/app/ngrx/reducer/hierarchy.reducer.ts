import { FetchHierarchyForId, FetchHierarchyForIdSuccess, FetchHierarchyForIdFailure } from './../action/hierarchy.actions';
import { createReducer, on } from '@ngrx/store';
import { initialHierarchyState } from '../state/hierarchy.state';

export const HierarchyReducer = createReducer(
    initialHierarchyState,

    on(FetchHierarchyForId, state => ({ ...state, loading: true, loaded: false })),

    on(FetchHierarchyForIdSuccess, (state, { total, hierarchy }) => ({ ...state, hierarchy, loaded: true, loading: false })),

    on(FetchHierarchyForIdFailure, state => ({ ...state, loading: false, loaded: true }))
);
