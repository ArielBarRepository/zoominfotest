import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { Observable, of } from 'rxjs';
import { Action } from '@ngrx/store';
import { catchError, map, mergeMap } from 'rxjs/operators';

import * as _ from 'lodash';
import { HierarchyService } from 'src/app/pages/services/hierarchy.service';
import { FetchHierarchyForId, FetchHierarchyForIdFailure, FetchHierarchyForIdSuccess } from '../action/hierarchy.actions';

@Injectable()
export class HierarchyEffects {

    constructor(private actions$: Actions,
        private hierarchyService: HierarchyService) { }

    public fetchHierarchyForId$ = createEffect((): Observable<Action> =>
        this.actions$
            .pipe(
                ofType(FetchHierarchyForId),
                mergeMap((action) => {
                    return this.hierarchyService.getHierarchyForId(action.id)
                        .pipe(
                            map((resp) => FetchHierarchyForIdSuccess({ total: _.get(resp, 'total'), hierarchy: _.get(resp, 'hierarchy') })),
                            catchError((err) => of(FetchHierarchyForIdFailure()))
                        );
                })
            ));
}
