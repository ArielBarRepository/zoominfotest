const dbService = require('./../db/db.service');

async function getHierarchyForId(id) {
   let hierarchyObj = [];
   await buildHierarchyForId(id, hierarchyObj);
   return {
      ...hierarchyObj,
      hierarchy: hierarchyObj.map(member => {
         return { id: member.memberId, name: member.name }
      })
   };
}

async function buildHierarchyForId(id, hierarchy) {
   const item = await dbService.getHierarchyItemById(id);
   hierarchy.unshift(item);
   if (item?.level > 1) {
      await buildHierarchyForId(item.parentMemberId, hierarchy);
   }
   else {
      if (hierarchy?.length > 0) {
         hierarchy.splice(hierarchy.length - 1, 1);
      }
   }

}


function getHierarchyStr(hierarchy) {
   var hierarchyStr = '';
   for (let i = 0; i < hierarchy.length; i++) {
      hierarchyStr += hierarchy[i].name + (i != hierarchy.length - 1 ? " ->    " : '');
   }
   return hierarchyStr;
}

getHierarchyForId(5);

module.exports = {
   getHierarchyForId
}
