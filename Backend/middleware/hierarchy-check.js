const controller = require('../hierarchy/hierarchy.controller');

module.exports = async function (req, res, next) {

    const accountId = res.locals.account.id;
    const hierarchyForId = await controller.getHierarchyForId(req.params.id);

    if (hierarchyForId.hierarchy.find(m => m.id == accountId) == undefined ) {
        return res.status(401).json({message: 'User cannot access to member details'});
    }

    next();
}